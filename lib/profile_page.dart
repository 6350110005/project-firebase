import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pro/about.dart';
import 'package:pro/chart.dart';
import 'package:pro/firestore.dart';

import 'package:pro/login_page.dart';

class ProfilePage extends StatefulWidget {

  final User user;

  const ProfilePage({required this.user});

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool _isSigningOut = false;

  late User currentUser;

  @override
  void initState() {
    currentUser = widget.user;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('My Music',style: GoogleFonts.alatsi(fontSize: 30)),
      ),

      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountEmail: Text("${currentUser.email}",style: GoogleFonts.alatsi(fontSize: 15),),
              accountName: Text("${currentUser.displayName}",style: GoogleFonts.alatsi(fontSize: 25),),
              currentAccountPicture: CircleAvatar(
                child: Image.asset("assets/images/146031.png"),
              ),
            ),
            ListTile(
              leading: Icon(Icons.accessibility_new_rounded,size: 30,),
              title: Text("Porfile",style: GoogleFonts.alatsi(fontSize: 25),),
              onTap: (){
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.area_chart,size: 30,),
              title: Text("Chart",style: GoogleFonts.alatsi(fontSize: 25),),
              onTap: (){
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Chart(),)
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.history_edu,size: 30,),
              title: Text("Firestore",style: GoogleFonts.alatsi(fontSize: 25),),
              onTap: (){
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => FireStore(),)
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.account_box_sharp,size: 30,),
              title: Text("About",style: GoogleFonts.alatsi(fontSize: 25),),
              onTap: (){
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => About(),)
                );
              },
            ),
          ],
        ),
      ),

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                Image.asset("assets/images/146031.png",height: 230,),
                SizedBox(height: 16,),
              ],
            ),
            Text(
              'Name: ${currentUser.displayName}',
              style: GoogleFonts.alegreya(fontWeight: FontWeight.bold,fontSize: 22),
            ),
            SizedBox(height: 16.0),
            Text(
              'Email: ${currentUser.email}',
              style: GoogleFonts.alegreya(fontWeight: FontWeight.bold,fontSize: 22),

            ),
            SizedBox(height: 16.0),
            _isSigningOut
                ? CircularProgressIndicator()
                : ElevatedButton(
              onPressed: () async {
                setState(() {
                  _isSigningOut = true;
                });
                await FirebaseAuth.instance.signOut();
                setState(() {
                  _isSigningOut = false;
                });
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginPage(),
                  ),
                );
              },
              child: Text('Sign out'),
              style: ElevatedButton.styleFrom(
                primary: Colors.indigo,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}







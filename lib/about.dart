import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:pro/chart.dart';
import 'package:pro/firestore.dart';
import 'package:pro/profile_page.dart';

class About extends StatefulWidget {
  const About({Key? key}) : super(key: key);

  @override
  State<About> createState() => _AboutState();
}

class _AboutState extends State<About> {

  late User currentUser;
  
  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      centerTitle: true,
      title: Text('About',style: GoogleFonts.alatsi(fontSize: 30),),
    ),
    body: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircleAvatar(
            radius: 70,
            backgroundImage: AssetImage("assets/images/IMG_20220718_160420.jpg",),
          ),
          SizedBox(height: 15,),
          Center(
            child: Text(
              'Name: Natthanon Songsrijan',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 10,),
          Center(
            child: Text(
              'Student ID: 6350110005',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 15,),
          CircleAvatar(
            radius: 70,
            backgroundImage: AssetImage("assets/images/310939134_549382623657911_728463455895092601_n.png",),
          ),
          SizedBox(height: 10,),
          Center(
            child: Text(
              'Name: Thammasatid Saengjun',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 10,),
          Center(
            child: Text(
              'Student ID: 6350110026',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 15,),
          CircleAvatar(
            radius: 70,
            backgroundImage: AssetImage("assets/images/310895886_1263210864257890_4158930317242574018_n.png",),
          ),
          SizedBox(height: 10,),
          Center(
            child: Text(
              'Name: Suthipat Wattanasin',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 10,),
          Center(
            child: Text(
              'Student ID: 6350110028',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 10,),
        ],
      ),
    ),
  );
}



